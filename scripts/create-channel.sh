#export binary from sample project
export PATH=/home/noureach/fabric-samples/bin:$PATH

#export CORE_PEER_MSPCONFIGPATH
export CORE_PEER_MSPCONFIGPATH=/home/noureach/test/crypto-config/peerOrganizations/acme.com/users/Admin@acme.com/msp

#create channel
peer channel create -o localhost:7050 -c mychannel -f ./channel-artifacts/channel.tx

configtxgen -profile AcmeOrdererGenesis -channelID myblock -outputBlock ./channel-artifacts/genesis.block
configtxgen -profile AcmeChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID mychannel
configtxgen -profile AcmeChannel -outputAnchorPeersUpdate ./channel-artifacts/anchor-org1.tx -channelID mychannel -asOrg Org1
configtxgen -profile AcmeChannel -outputAnchorPeersUpdate ./channel-artifacts/anchor-org2.tx -channelID mychannel -asOrg Org2

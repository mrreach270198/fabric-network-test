# Initializes the node
WAIT_TIME=4s
ORDERER_ADDRESS="localhost:7050"

#1.  Kill the peer process if it is running
killall peer

# All config stored under the configtxgen folder
export  CONFIG_DIRECTORY=$PWD

export FABRIC_CFG_PATH=$PWD

# Change this to an appropriate level
# export CORE_LOGGING_LEVEL=INFO
export FABRIC_LOGGING_SPEC=INFO

# Variables for setting peer addresses
export CORE_PEER_LISTENADDRESS=0.0.0.0:7053
export CORE_PEER_ADDRESS=localhost:7053
# export CORE_PEER_TLS_ENABLED = true

# Change this to folder for managing the ledger
# You may use the following to point to the current folder
# Be aware that GoLevelDB does not work well with mounted file systems so you may see
# errors in using the folder that is on host system.
export CORE_PEER_FILESYSTEMPATH=$HOME/test/ledger

# Identity set to Admin
export CORE_PEER_MSPCONFIGPATH=/home/noureach/test/crypto-config/ordererOrganizations/acme.com/users/Admin@acme.com/msp
echo "Configured !!" + $CORE_PEER_MSPCONFIGPATH

# State Data persistence in CouchDB
CORE_LEDGER_STATE_STATEDATABASE=CouchDB
# For Couch DB
CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=localhost:5984
CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=admin
CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password

export NODECHAINCODE=/vagrant/nodechaincode

# Default Chaincode variables
export CC_NAME=gocc
export CC_VERSION=1.0
export CC_LABEL="$CC_NAME.$CC_VERSION-1.0"
export CC_PACKAGE_FILE=$HOME/packages/$CC_LABEL.tar.gz


#6 Launch the peer in background
peer node start  &

#7 Wait for the peer to launch
echo   '4. Sleeping for WAIT_TIME'
sleep   $WAIT_TIME

#8 Join the channel
echo   '5. Joining channel'
peer channel join -b ./mychannel.block

#9 Check if the join was successful
echo   '6. Listing channel'
peer channel list

#10 Kill the peer
# echo   '7. Kill the peer process'
# killall peer

# echo "All Steps Executed."
# echo "Launch Peer using ./start-node.sh"